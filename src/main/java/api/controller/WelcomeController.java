package api.controller;

import api.dto.Token;
import api.dto.manyToMany.StudentDTO;
import api.dto.oneToMany.ExampleDTO;
import api.dto.oneToOne.ProductDTO;
import api.entity.user.AuthRequest;
import api.repository.UserRepository;
import api.service.ExampleServicers;
import api.service.ProductServices;
import api.service.StudentServicers;
import api.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WelcomeController {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private ProductServices productServices;
    @Autowired
    private ExampleServicers exampleServicers;
    ///
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StudentServicers studentServicers;

    @GetMapping("/demo")
    public String ss() {
        return "demo hihihi!!";
    }

    @GetMapping("/user")
    public String welcome() {
        return "Welcome to User!!";
    }

    @GetMapping("/admin")
    public String huy() {
        return "Welcome to Admin !!";
    }

    @PostMapping("/admin")
    public String demo() {
        return "ahihi";
    }

    @PostMapping("/authenticate")
    public ResponseEntity<Token> generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword())
            );
        } catch (Exception ex) {
            throw new Exception("inavalid username/password");
        }
        return ResponseEntity.ok(new Token(jwtUtil.generateToken(authRequest.getUserName())));
    }

    //One to one
    @PostMapping("/product")
    public ResponseEntity<Boolean> createData(@RequestBody ProductDTO productDTO) {
        return ResponseEntity.ok(productServices.create(productDTO));
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Boolean> upadteData(@PathVariable Long id, @RequestBody ProductDTO productDTO) {
        return ResponseEntity.ok(productServices.update(id, productDTO));
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable Long id) {
        return ResponseEntity.ok(productServices.delete(id));
    }

    @GetMapping("/product")
    public ResponseEntity<List<ProductDTO>> findAll() {
        return ResponseEntity.ok(productServices.findAll());
    }

    @GetMapping("/product-query")
    public ResponseEntity<List<ProductDTO>> findAllQuery() {
        return ResponseEntity.ok(productServices.findAllByQuery());
    }
    @PostMapping("/product-query")
    public ResponseEntity<Boolean> createDataQuery(@RequestBody ProductDTO productDTO) {
        return ResponseEntity.ok(productServices.insert_query(productDTO));
    }
    @PutMapping("/product-query/{id}")
    public ResponseEntity<Boolean> updateDataQuery(@PathVariable Long id,@RequestBody ProductDTO productDTO){
        return ResponseEntity.ok(productServices.update_query(id,productDTO));
    }
    @DeleteMapping("/product-query/{id}")
    public ResponseEntity<Boolean> deleteDataQuery(@PathVariable Long id){
        return ResponseEntity.ok(productServices.delete_query(id));
    }

    //One To Many
    @GetMapping("/example")
    public ResponseEntity<List<ExampleDTO>> find (){
        return ResponseEntity.ok(exampleServicers.findAll());
    }
    @PostMapping("/example")
    public ResponseEntity<Boolean> cre(@RequestBody ExampleDTO exampleDTO){
        return ResponseEntity.ok(exampleServicers.create(exampleDTO));
    }
    @DeleteMapping("/example/{id}")
    public ResponseEntity<Boolean> dele(@PathVariable Long id){
        return ResponseEntity.ok(exampleServicers.delete(id));
    }
    @PutMapping("/example/{id}")
    public ResponseEntity<Boolean> upda(@PathVariable Long id,@RequestBody ExampleDTO exampleDTO){
        return ResponseEntity.ok(exampleServicers.update(id,exampleDTO));
    }

    @GetMapping("/example-query")
    public ResponseEntity<List<ExampleDTO>> findQuery (){
        return ResponseEntity.ok(exampleServicers.find_by_query());
    }
    @PostMapping("/example-query")
    public ResponseEntity<Boolean> crequery(@RequestBody ExampleDTO exampleDTO){
        return ResponseEntity.ok(exampleServicers.insert_by_query(exampleDTO));
    }
    @DeleteMapping("/example-query/{id}")
    public ResponseEntity<Boolean> delequery(@PathVariable Long id){
        return ResponseEntity.ok(exampleServicers.delete_by_query(id));
    }
    @PutMapping("/example-query/{id}")
    public ResponseEntity<Boolean> updaquery(@PathVariable Long id,@RequestBody ExampleDTO exampleDTO){
        return ResponseEntity.ok(exampleServicers.update_by_query(id,exampleDTO));
    }
    //many to many
    //truyền id và course nếu đó là course đã có trong db
    @PostMapping("/student")
    public ResponseEntity<Boolean> crea(@RequestBody StudentDTO studentDTO){
        return ResponseEntity.ok(studentServicers.create(studentDTO));
    }
    @GetMapping("/student")
    public ResponseEntity<List<StudentDTO>> getAll(){
        return ResponseEntity.ok(studentServicers.findAll());
    }
    @DeleteMapping("/student/{id}")
    public ResponseEntity<Boolean> deleteStudent(@PathVariable Long id){
        return ResponseEntity.ok(studentServicers.delete(id));
    }
    @PutMapping("/student/{id}")
    public ResponseEntity<Boolean> updateStudent(@PathVariable Long id, @RequestBody StudentDTO studentDTO){
        return ResponseEntity.ok(studentServicers.update(id,studentDTO));
    }

    @PostMapping("/student-query")
    public ResponseEntity<Boolean> creaquery(@RequestBody StudentDTO studentDTO){
        return ResponseEntity.ok(studentServicers.insert_query(studentDTO));
    }
    @GetMapping("/student-query")
    public ResponseEntity<List<StudentDTO>> getAllquery(){
        return ResponseEntity.ok(studentServicers.find_by_query());
    }
    @DeleteMapping("/student-query/{id}")
    public ResponseEntity<Boolean> deleteStudentquery(@PathVariable Long id){
        return ResponseEntity.ok(studentServicers.delete_query(id));
    }
    @PutMapping("/student-query/{id}")
    public ResponseEntity<Boolean> updateStudentquery(@PathVariable Long id, @RequestBody StudentDTO studentDTO){
        return ResponseEntity.ok(studentServicers.update_query(id,studentDTO));
    }

}
