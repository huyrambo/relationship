package api.service;

import api.dto.oneToOne.ProductDTO;
import api.entity.oneToOne.ProductEntity;
import api.mapping.oneToOne.mappingOTOimp;
import api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ProductServices {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private mappingOTOimp mappingOTOimp;

    public boolean create(ProductDTO productDTO) {
    productRepository.save(mappingOTOimp.mappingDTOtoEntity(productDTO));

        return true;
    }

    public List<ProductDTO> findAll(){
        List<ProductEntity> list = productRepository.findAll();
        List<ProductDTO> productDTOS = new ArrayList<>();
        for (ProductEntity productEntity: list
             ) {
            ProductDTO productDTO = mappingOTOimp.mappingEntityToDto(productEntity);
            productDTOS.add(productDTO);
        }
        return productDTOS;
    }
    public boolean delete (Long id){
        Optional<ProductEntity> productEntity = productRepository.findById(id);
        if (!productEntity.isPresent()) return false;
        else {
            productRepository.delete(productEntity.get());
            return true;
        }
    }
    public boolean update (Long id, ProductDTO productDTO){
        Optional<ProductEntity> productEntity = productRepository.findById(id);
        if (!productEntity.isPresent()) return false;
        else {
            ProductEntity productEntity1 = mappingOTOimp.mappingDTOtoEntity(productDTO);
            ProductEntity productEntity2 = productEntity.get();
            productEntity1.setId(id);
            productEntity1.getTest().setId(productEntity2.getTest().getId());
            productRepository.save(productEntity1);
            return true;
        }
    }

    //query
    public List<ProductDTO> findAllByQuery(){
        List<ProductEntity> list = productRepository.get_all();
        List<ProductDTO> productDTOS = new ArrayList<>();
        for (ProductEntity productEntity: list
        ) {
            ProductDTO productDTO = mappingOTOimp.mappingEntityToDto(productEntity);
            productDTOS.add(productDTO);
        }
        return productDTOS;
    }
    public boolean insert_query(ProductDTO productDTO){
        String test_name = productDTO.getTest().getName();
        String name = productDTO.getName();
        String des = productDTO.getDesription();
        productRepository.insert_query(test_name,name,des);
        return true;
    }
    public boolean update_query(Long id,ProductDTO productDTO){
        productRepository.update_query(id,productDTO.getName(),productDTO.getDesription(),productDTO.getTest().getName());
        return true;
    }
    public boolean delete_query(Long id){
        productRepository.delete_query(id);
        return true;
    }


}
