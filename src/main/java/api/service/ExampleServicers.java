package api.service;

import api.dto.oneToMany.ExampleDTO;
import api.dto.oneToMany.QuestionsDTO;
import api.dto.oneToOne.ProductDTO;
import api.entity.oneToMany.Example;
import api.mapping.oneToMany.mappingOTMimp;
import api.repository.ExampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ExampleServicers {
    @Autowired
    private ExampleRepository exampleRepository;
    @Autowired
    private mappingOTMimp mappingOTMimp;
    public List<ExampleDTO> findAll(){
        List<ExampleDTO> exampleDTOS = new ArrayList<>();
        List<Example> examples = new ArrayList<>();
        examples = exampleRepository.findAll();
        for (Example example:examples
             ) {
            ExampleDTO exampleDTO= mappingOTMimp.mappingEntityToDTo(example);
            exampleDTOS.add(exampleDTO);
        }
        return exampleDTOS;
    }
    public boolean create(ExampleDTO exampleDTO){
         exampleRepository.save(mappingOTMimp.mappingDtoToEntity(exampleDTO));
         return true;
    }
    public boolean delete(Long id){
        Optional<Example> example = exampleRepository.findById(id);
        if (!example.isPresent()) return false;
        else{
            exampleRepository.delete(example.get());
            return true;
        }
    }
    public  boolean update (Long id, ExampleDTO exampleDTO){
        Optional<Example> example = exampleRepository.findById(id);
        if (!example.isPresent()) return false;
        else {
            exampleDTO.setId(id);
            List<QuestionsDTO> questionsDTOS = exampleDTO.getQuestions();
            exampleDTO.setQuestions(mappingOTMimp.mappingList(example.get().getQuestions(),questionsDTOS));
            exampleRepository.save(mappingOTMimp.mappingDtoToEntity(exampleDTO));
            return true;
        }
    }

    //query
    public String createStringAnserQuestions (List<QuestionsDTO> questionsDTOS){
        String str = "";
        for (int i = 0; i < questionsDTOS.size(); i++) {

            if (i==questionsDTOS.size()-1){
                str += questionsDTOS.get(i).getAnswer();
            }
            else{
                str += questionsDTOS.get(i).getAnswer()+ ",";
            }
        }
        return str;
    }
    public String createStringQuestions (List<QuestionsDTO> questionsDTOS){
        String str = "";
        for (int i = 0; i < questionsDTOS.size(); i++) {

            if (i==questionsDTOS.size()-1){
                str += questionsDTOS.get(i).getId()+ ","+questionsDTOS.get(i).getAnswer();
            }
            else{
                str += questionsDTOS.get(i).getId()+ ","+questionsDTOS.get(i).getAnswer()+",";
            }
        }
        return str;
    }
    public List<ExampleDTO> find_by_query(){
        List<ExampleDTO> exampleDTOS = new ArrayList<>();
        List<Example> examples = new ArrayList<>();
        examples = exampleRepository.find_all_query();
        for (Example example:examples
        ) {
            ExampleDTO exampleDTO= mappingOTMimp.mappingEntityToDTo(example);
            exampleDTOS.add(exampleDTO);
        }
        return exampleDTOS;
    }
    public boolean insert_by_query(ExampleDTO exampleDTO){
        String answer = createStringAnserQuestions(exampleDTO.getQuestions());
        exampleRepository.insert_query(exampleDTO.getDesription(),exampleDTO.getTitle(),answer);
        return true;
    }
    public boolean update_by_query(Long id, ExampleDTO exampleDTO){
        String answer = createStringQuestions(exampleDTO.getQuestions());
        exampleRepository.update_query(id,exampleDTO.getDesription(),exampleDTO.getTitle(),answer);
        return true;
    }
    public boolean delete_by_query(Long id){
        exampleRepository.delete_insert(id);
        return true;
    }
}
