package api.service;

import api.dto.manyToMany.CourseDTO;
import api.dto.manyToMany.StudentDTO;
import api.entity.manyToMany.Course;
import api.entity.manyToMany.Student;
import api.mapping.manyToMany.mappingMTMimp;
import api.repository.manyTomany.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class StudentServicers {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private mappingMTMimp mappingMTMimp;
    public boolean create(StudentDTO studentDTO){

        studentRepository.save(mappingMTMimp.mappingDTOToEntity(studentDTO));
        return true;
    }
    public List<StudentDTO> findAll(){

        List<Student> students = studentRepository.findAll();
        List<StudentDTO> studentDTOS = new ArrayList<>();
        for (Student student:students
             ) {
            StudentDTO studentDTO = mappingMTMimp.mappingEntityToDTO(student);
            studentDTOS.add(studentDTO);
        }
        return studentDTOS;
    }

    public boolean delete(Long id){

        Optional<Student> student = studentRepository.findById(id);
        if (!student.isPresent()) return false;
        else {
            Student student1 = student.get();
            for (Course course: student1.getCourses()
                 ) {
                course.getStudents().remove(student1);
            }
//            student1.getCourses().remove(student1);
            student1.setCourses(null);
            studentRepository.delete(student1);
        }
        return true;
    }
    public boolean update(Long id,StudentDTO studentDTO){
        Student student=mappingMTMimp.mappingDTOToEntityUpdate(studentDTO,id);
        studentRepository.save(student);
        return true;
    }
    //query
    public String createStringCourse(Set<CourseDTO> courseDTOS){
        String str = "";
        int count = 0;
        for (CourseDTO co:courseDTOS
             ) {
            count++;
            if (count ==courseDTOS.size()){
                str += co.getId()+ ","+co.getName();
            }
            else {
                str += co.getId() + "," + co.getName() + ",";
            }
        }
        return str;
    }
    public List<StudentDTO> find_by_query(){
        List<Student> students = studentRepository.find_all_query();
        List<StudentDTO> studentDTOS = new ArrayList<>();
        for (Student student:students
        ) {
            StudentDTO studentDTO = mappingMTMimp.mappingEntityToDTO(student);
            studentDTOS.add(studentDTO);
        }
        return studentDTOS;
    }
    public boolean insert_query(StudentDTO studentDTO){
        String str= createStringCourse(studentDTO.getCourses());
        studentRepository.insert_by_query(studentDTO.getName(),str);
        return true;
    }
    public boolean delete_query(Long id){
        studentRepository.delete_by_query(id);
        return true;
    }
    public boolean update_query(Long id,StudentDTO studentDTO){
        String str = createStringCourse(studentDTO.getCourses());
        studentRepository.update_by_query(id,studentDTO.getName(),str);
        return true;
    }
}
