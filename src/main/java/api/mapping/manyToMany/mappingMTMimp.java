package api.mapping.manyToMany;

import api.dto.manyToMany.StudentDTO;
import api.entity.manyToMany.Student;

public interface mappingMTMimp {
    public StudentDTO mappingEntityToDTO(Student student);
    public Student mappingDTOToEntity (StudentDTO studentDTO);
    public Student mappingDTOToEntityUpdate(StudentDTO studentDTO,Long id);
}
