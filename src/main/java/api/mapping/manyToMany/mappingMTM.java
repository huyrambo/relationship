package api.mapping.manyToMany;

import api.dto.manyToMany.CourseDTO;
import api.dto.manyToMany.StudentDTO;
import api.entity.manyToMany.Course;
import api.entity.manyToMany.Student;
import api.repository.manyTomany.CourseRepository;
import api.repository.manyTomany.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class mappingMTM implements mappingMTMimp {
    @Autowired
    private CourseRepository courseRepository;

    @Override
    public StudentDTO mappingEntityToDTO(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(student.getId());
        studentDTO.setName(student.getName());
        Set<CourseDTO> courseDTOS = new HashSet<>();
        for (Course course : student.getCourses()
        ) {
            CourseDTO courseDTO = new CourseDTO();
            courseDTO.setId(course.getId());
            courseDTO.setName(course.getName());
            courseDTOS.add(courseDTO);
        }
        studentDTO.setCourses(courseDTOS);
        return studentDTO;
    }

    @Override
    public Student mappingDTOToEntity(StudentDTO studentDTO) {
        Student student = new Student();
        student.setName(studentDTO.getName());
        Set<Student> students = new HashSet<>();
        students.add(student);
        Set<Course> courses = new HashSet<>();
        for (CourseDTO courseDTO : studentDTO.getCourses()
        ) {
            if (courseDTO.getId() != null) {
                Course course = courseRepository.findById(courseDTO.getId()).get();
                course.getStudents().add(student);
                courses.add(course);
            } else {
                Course course = new Course();
                course.setName(courseDTO.getName());
                course.setId(courseDTO.getId());
                course.setStudents(students);
                courses.add(course);
            }
        }
        student.setCourses(courses);
        return student;
    }

    @Override
    public Student mappingDTOToEntityUpdate(StudentDTO studentDTO, Long id) {
        Student student = new Student();
        student.setId(id);
        student.setName(studentDTO.getName());
        Set<Student> students = new HashSet<>();
        students.add(student);
        Set<Course> courses = new HashSet<>();
        for (CourseDTO courseDTO : studentDTO.getCourses()
        ) {
            if (courseDTO.getId() != null) {
                Course course = courseRepository.findById(courseDTO.getId()).get();
                try {
                for (Student student1: course.getStudents()
                     ) {
                        if (student1.getId()==id) course.getStudents().remove(student1);
                }
                }catch (Exception e){

                }
                course.getStudents().add(student);
                courses.add(course);
            } else {
                Course course = new Course();
                course.setName(courseDTO.getName());
                course.setId(courseDTO.getId());
                course.setStudents(students);
                courses.add(course);
            }
        }
        student.setCourses(courses);
        return student;
    }


}
