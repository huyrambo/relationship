package api.mapping.oneToMany;

import api.dto.oneToMany.ExampleDTO;
import api.dto.oneToMany.QuestionsDTO;
import api.entity.oneToMany.Example;
import api.entity.oneToMany.Questions;

import java.util.List;

public interface mappingOTMimp {
    public ExampleDTO mappingEntityToDTo(Example example);
    public Example mappingDtoToEntity (ExampleDTO exampleDTO);
    public List<QuestionsDTO> mappingList(List<Questions> list, List<QuestionsDTO> listDTO);
}
