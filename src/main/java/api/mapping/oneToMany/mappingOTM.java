package api.mapping.oneToMany;

import api.dto.oneToMany.ExampleDTO;
import api.dto.oneToMany.QuestionsDTO;
import api.entity.oneToMany.Example;
import api.entity.oneToMany.Questions;
import api.repository.QuestionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class mappingOTM implements mappingOTMimp {
    @Autowired
    private QuestionsRepository questionsRepository;

    @Override
    public ExampleDTO mappingEntityToDTo(Example example) {
        ExampleDTO exampleDTO = new ExampleDTO();
        exampleDTO.setId(example.getId());
        exampleDTO.setTitle(example.getTitle());
        exampleDTO.setDesription(example.getDesription());
        List<QuestionsDTO> questionsDTOS = new ArrayList<>();
        for (Questions questions : example.getQuestions()
        ) {
            QuestionsDTO questionsDTO = new QuestionsDTO();
            questionsDTO.setAnswer(questions.getAnswer());
            questionsDTO.setId(questions.getId());
            questionsDTOS.add(questionsDTO);
        }
        exampleDTO.setQuestions(questionsDTOS);
        return exampleDTO;
    }

    @Override
    public Example mappingDtoToEntity(ExampleDTO exampleDTO) {
        Example example = new Example();
        example.setId(exampleDTO.getId());
        example.setTitle(exampleDTO.getTitle());
        example.setDesription(exampleDTO.getDesription());
        List<Questions> questions = new ArrayList<>();
        for (QuestionsDTO questionsDTO : exampleDTO.getQuestions()
        ) {
            Questions questions1 = new Questions();
            questions1.setAnswer(questionsDTO.getAnswer());
            questions1.setExample(example);
            questions1.setId(questionsDTO.getId());
            questions.add(questions1);
        }
        example.setQuestions(questions);
        return example;
    }

    @Override
    public List<QuestionsDTO> mappingList(List<Questions> list, List<QuestionsDTO> listDTO) {
        int sizeIn = list.size();
        int sizeOut = listDTO.size();
        if (sizeIn == sizeOut || sizeIn < sizeOut) {
            for (int i = 0; i < sizeIn; i++) {
                listDTO.get(i).setId(list.get(i).getId());
            }

        } else {
            for (int i = 0; i < sizeOut; i++) {
                listDTO.get(i).setId(list.get(i).getId());
            }
            for (int i = sizeOut; i < sizeIn; i++) {

                Questions questions = list.get(i);
                questions.setExample(null);
                questionsRepository.delete(questions);
            }

        }
        return listDTO;
    }


}
