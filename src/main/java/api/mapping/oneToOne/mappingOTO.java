package api.mapping.oneToOne;

import api.dto.oneToOne.ProductDTO;
import api.dto.oneToOne.TestDTO;
import api.entity.oneToOne.ProductEntity;
import api.entity.oneToOne.Test;
import org.springframework.stereotype.Component;


@Component
public class mappingOTO implements mappingOTOimp{
    @Override
    public ProductDTO mappingEntityToDto(ProductEntity productEntity) {
         ProductDTO productDTO =new ProductDTO();
         productDTO.setId(productEntity.getId());
         productDTO.setDesription(productEntity.getDesription());
         productDTO.setName(productEntity.getName());
        TestDTO testDTO =new TestDTO();
        testDTO.setId(productEntity.getTest().getId());
        testDTO.setName(productEntity.getTest().getName());
        productDTO.setTest(testDTO);
        return productDTO;

    }

    @Override
    public ProductEntity mappingDTOtoEntity(ProductDTO productDTO) {
        ProductEntity productEntity =new ProductEntity();
        productEntity.setName(productDTO.getName());
        productEntity.setDesription(productDTO.getDesription());
        Test test =new Test();
        test.setName(productDTO.getTest().getName());
        test.setProduct(productEntity);
        productEntity.setTest(test);
        return productEntity;
    }
}
