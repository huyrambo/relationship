package api.mapping.oneToOne;

import api.dto.oneToOne.ProductDTO;
import api.entity.oneToOne.ProductEntity;



public interface mappingOTOimp {
    public ProductDTO mappingEntityToDto(ProductEntity productEntity);
    public ProductEntity mappingDTOtoEntity(ProductDTO productDTO);
}
