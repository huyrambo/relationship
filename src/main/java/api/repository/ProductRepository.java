package api.repository;

import api.entity.oneToOne.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Query(value = "SELECT * FROM product_entity JOIN test ON product_entity.test_id = test.id",nativeQuery = true)
    public List<ProductEntity> get_all();
//    CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_product`(IN test_name varchar(255),IN name_product varchar(255), IN des varchar(255) )
//    BEGIN
//    insert into test (name) values (test_name);
//    insert into product_entity (name,desription,test_id) values(name_product,des,LAST_INSERT_ID());
//    END
    @Modifying
    @Transactional
    @Query(value = "call insert_product(:test_name,:name_product,:des)",nativeQuery = true)
    public void insert_query(@Param("test_name") String test_name,@Param("name_product") String name,@Param("des") String desription);
    @Modifying
    @Transactional
    @Query(value = "UPDATE product_entity\n" +
            "INNER JOIN  test ON product_entity.test_id = test.id\n" +
            "SET product_entity.name = :name, \n" +
            "    product_entity.desription = :desription,\n" +
            "    test.name= :testname\n" +
            "WHERE product_entity.id=:id",nativeQuery = true)
    public void update_query(@Param("id") Long id,@Param("name") String name, @Param("desription") String desription,@Param("testname") String testname);
    @Modifying
    @Transactional
    @Query(value = "DELETE product_entity, test \n" +
            "FROM test\n" +
            "INNER JOIN product_entity ON test.id = product_entity.test_id\n" +
            "WHERE product_entity.id = :id",nativeQuery = true)
    public void delete_query(@Param("id") Long id);
}
