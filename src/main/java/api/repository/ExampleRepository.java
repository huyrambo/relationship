package api.repository;

import api.entity.oneToMany.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface ExampleRepository extends JpaRepository<Example,Long> {
    @Query(value = "SELECT * FROM example LEFT JOIN questions ON example.id = questions.example_id group by example.id ",nativeQuery = true)
    public List<Example> find_all_query();
//    CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_example`(in desription varchar(255), in title varchar(255),in value_questions text)
//    BEGIN
//    declare id int default 0;
//    DECLARE str text DEFAULT value_questions;
//    declare answer varchar(255) default '';
//    insert into example(desription,title) values(desription,title);
//    set id = (LAST_INSERT_ID());
//    loop_label:  LOOP
//    set answer=(substring_index(str,',',1));
//    set str = (substring(str,length(answer)+2));
//    insert into questions (answer,example_id) values (answer,id);
//    IF  str='' THEN
//    LEAVE  loop_label;
//    END  IF;
//    END LOOP;
//    END
    @Modifying
    @Transactional
    @Query(value = "call insert_example(:desription,:title,:value_questions)",nativeQuery = true)
    public void insert_query(@Param("desription") String desription,@Param("title") String title,@Param("value_questions") String value_questions);

    @Modifying
    @Transactional
    @Query(value = "DELETE example, questions\n" +
            "FROM example\n" +
            "LEFT JOIN questions ON example.id = questions.example_id\n" +
            "WHERE example.id = :id",nativeQuery = true)
    public void delete_insert(@Param("id") Long id);
    //    CREATE DEFINER=`root`@`localhost` PROCEDURE `update_example`(in id_example int, in desription varchar(255),in title varchar(255),in questions text )
//    BEGIN
//    declare quest  text default questions;
//    declare quest_id varchar(255) default '';
//    declare answer varchar(255) default '';
//    update example set desription = desription,title=title where id = id_example;
//    loop_label:  LOOP
//    set quest_id =(substring_index(quest,',',1));
//    set quest = (substring(quest,length(quest_id)+2));
//    set answer=(substring_index(quest,',',1));
//    set quest = (substring(quest,length(answer)+2));
//if quest_id = 'null' then
//    insert into questions(answer, example_id) values(answer,id_example);
//   else
//    update questions set answer = answer where id = quest_id and example_id= id_example;
//    end if;
//    IF  quest='' THEN
//    LEAVE  loop_label;
//    END  IF;
//    END LOOP;
//
//    END
    @Modifying
    @Transactional
    @Query(value = "call update_example(:id_example,:desription,:title,:questions)",nativeQuery = true)
    public void update_query(@Param("id_example") Long id,@Param("desription") String desription,@Param("title") String title,@Param("questions") String questions);

}
