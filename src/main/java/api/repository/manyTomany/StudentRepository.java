package api.repository.manyTomany;

import api.entity.manyToMany.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
    @Query(value = "SELECT student.id, student.name, course.id as course_id,course.name\n" +
            "FROM ((course_like\n" +
            "right JOIN student ON student.id = course_like.student_id)\n" +
            "INNER JOIN course ON course.id = course_like.course_id) group by student.id;",nativeQuery = true)
    public List<Student> find_all_query();
//    CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_student`(in name_student varchar(255), in courses text)
//    BEGIN
//    declare id_student int default 0;
//    declare course text default courses;
//    declare course_id varchar(255) default '';
//    declare course_name varchar(255) default '';
//    declare id_course int default 0;
//    insert into student(name) values(name_student);
//    set id_student = (last_insert_id());
//    loop_label:  LOOP
//    set course_id =(substring_index(course,',',1));
//    set course = (substring(course,length(course_id)+2));
//    set course_name=(substring_index(course,',',1));
//    set course = (substring(course,length(course_name)+2));
//if course_id = 'null' then
//    insert into course (name) values (course_name);
//    set id_course = (last_insert_id());
//    insert into course_like(student_id,course_id) values(id_student,id_course);
//else
//    insert into course_like(student_id,course_id) values(id_student,course_id);
//    end if;
//    IF  course='' THEN
//    LEAVE  loop_label;
//    END  IF;
//    END LOOP;
//    END
    @Modifying
    @Transactional
    @Query(value = "call insert_student(:name,:courses)",nativeQuery = true)
    public void insert_by_query(@Param("name") String name,@Param("courses") String courses);
//    CREATE DEFINER=`root`@`localhost` PROCEDURE `update_student`(in id_student int, in name_student varchar(255),in course text)
//    BEGIN
//
//    declare course text default course;
//    declare id_course varchar(255) default '';
//    declare name_course varchar(255) default '';
//    declare ids int default 0;
//    update student set name = name_student where id = id_student;
//    loop_label:  LOOP
//    set id_course =(substring_index(course,',',1));
//    set course = (substring(course,length(id_course)+2));
//    set name_course=(substring_index(course,',',1));
//    set course = (substring(course,length(name_course)+2));
//      if id_course ='null' then
//    insert into course(name) values(name_course);
//    set ids = (last_insert_id());
//    insert into course_like(student_id,course_id) values(id_student,ids);
//      else
//    update course set name = name_course where id = id_course;
//    end if;
//    IF  course='' THEN
//    LEAVE  loop_label;
//    END  IF;
//    END LOOP;
//
//    END
    @Modifying
    @Transactional
    @Query(value = "call update_student(:id,:name,:courses)",nativeQuery = true)
    public void update_by_query(@Param("id") Long id,@Param("name") String name,@Param("courses") String courses);
    @Modifying
    @Transactional
    @Query(value = "DELETE student, course_like\n" +
            "FROM student\n" +
            "inner JOIN course_like ON student.id = course_like.student_id\n" +
            "WHERE student.id=:id;",nativeQuery = true)
    public void delete_by_query(@Param("id") Long id);
}
