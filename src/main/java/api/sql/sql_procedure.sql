use security
DELIMITER $$
USE `security`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_example`(in desription varchar(255), in title varchar(255),in value_questions text)
BEGIN
   declare id int default 0;
   DECLARE str text DEFAULT value_questions;
declare answer varchar(255) default '';
insert into example(desription,title) values(desription,title);
set id = (LAST_INSERT_ID());
loop_label:  LOOP
set answer=(substring_index(str,',',1));
set str = (substring(str,length(answer)+2));
insert into questions (answer,example_id) values (answer,id);
		IF  str='' THEN 
			LEAVE  loop_label;
		END  IF;
	END LOOP;
END$$

DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_product`(IN test_name varchar(255),IN name_product varchar(255), IN des varchar(255) )
BEGIN
insert into test (name) values (test_name);
insert into product_entity (name,desription,test_id) values(name_product,des,LAST_INSERT_ID());
END
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_student`(in name_student varchar(255), in courses text)
BEGIN
declare id_student int default 0;
declare course text default courses;
declare course_id varchar(255) default '';
declare course_name varchar(255) default '';
declare id_course int default 0; 
insert into student(name) values(name_student);
set id_student = (last_insert_id());
loop_label:  LOOP
set course_id =(substring_index(course,',',1));
set course = (substring(course,length(course_id)+2));
set course_name=(substring_index(course,',',1));
set course = (substring(course,length(course_name)+2));
if course_id = 'null' then
insert into course (name) values (course_name);
set id_course = (last_insert_id());
insert into course_like(student_id,course_id) values(id_student,id_course);
else
insert into course_like(student_id,course_id) values(id_student,course_id);
end if;
		IF  course='' THEN 
			LEAVE  loop_label;
		END  IF;
	END LOOP;
END
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_example`(in id_example int, in desription varchar(255),in title varchar(255),in questions text )
BEGIN
declare quest  text default questions;
declare quest_id varchar(255) default '';
declare answer varchar(255) default '';
update example set desription = desription,title=title where id = id_example;
loop_label:  LOOP
set quest_id =(substring_index(quest,',',1));
set quest = (substring(quest,length(quest_id)+2));
set answer=(substring_index(quest,',',1));
set quest = (substring(quest,length(answer)+2));
if quest_id = 'null' then
   insert into questions(answer, example_id) values(answer,id_example);
   else
   update questions set answer = answer where id = quest_id and example_id= id_example;
   end if;
		IF  quest='' THEN 
			LEAVE  loop_label;
		END  IF;
	END LOOP;

END
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_student`(in id_student int, in name_student varchar(255),in course text)
BEGIN

declare course text default course;
declare id_course varchar(255) default '';
declare name_course varchar(255) default '';
declare ids int default 0;
   update student set name = name_student where id = id_student;
loop_label:  LOOP
set id_course =(substring_index(course,',',1));
set course = (substring(course,length(id_course)+2));
set name_course=(substring_index(course,',',1));
set course = (substring(course,length(name_course)+2));
      if id_course ='null' then
      insert into course(name) values(name_course);
      set ids = (last_insert_id());
      insert into course_like(student_id,course_id) values(id_student,ids);
      else
      update course set name = name_course where id = id_course;
      end if;
		IF  course='' THEN 
			LEAVE  loop_label;
		END  IF;
	END LOOP;
 
END
DELIMITER ;